//
//  TagReference.swift
//  technical-test-ios (iOS)
//
//  Created by Landony Aguilar on 22/09/21.
//

import Foundation

public class TagReference {
    static let tagUrlBaseProd = "https://api.unsplash.com"
    static let tagUrlBase = TagReference.tagUrlBaseProd
    
    static let tagUrlPhotos = "/photos"
    static let tagUrlUser = "/users/"
}
