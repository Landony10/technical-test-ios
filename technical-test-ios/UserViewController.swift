//
//  UserViewController.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit
import Kingfisher

class UserViewController: UIViewController {
    
    @IBOutlet weak var profileUserImage: RoundedWithoutBorderImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var totalPhotosLabel: UILabel!
    @IBOutlet weak var totalLikesLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    internal var userService: UserService!
    internal var user: User!
    
    private var photos: [PhotosUser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photosCollectionView.delegate = self
        self.photosCollectionView.dataSource = self
        // Do any additional setup after loading the view.
        self.setData()
        self.getUser()
    }
    
    private func setData() {
        let urlPhotoUser = URL(string: user.profile_image?.medium ?? "https://i.imgur.com/tGbaZCY.jpg")
        self.profileUserImage.kf.setImage(with: urlPhotoUser)
        self.nameLabel.text = "\(user.firstName ?? "") \(user.lastName ?? "")"
        self.descriptionLabel.attributedText = user.bio?.convertHtmlToAttributedStringWithCSS(font: UIFont(name: "Nunito-Regular", size: 15), csscolor: "#a7a9ac", lineheight: 5, csstextalign: "center")
        self.totalPhotosLabel.text = "\(user.totalPhotos ?? 0)"
        self.totalLikesLabel.text = "\(user.totalLikes ?? 0)"
        self.locationLabel.text = user.location ?? "No location"
    }
    
    private func getUser() {
        self.showLoader()
        self.userService.getUser(user: self.user.username ?? "") { data, statusCode in
            self.proccessDataUser(data: data)
        }
    }
    
    private func proccessDataUser(data: User?) {
        self.hideLoader()
        if data != nil {
            let photosUser = data?.photos ?? []
            
            if !photosUser.isEmpty {
                for photoUser in photosUser {
                    self.photos.append(photoUser)
                    self.photosCollectionView.reloadData()
                }
            }
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UserViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoUserCell", for: indexPath) as! PhotoUserCollectionViewCell
        let photo = self.photos[indexPath.row]
        let urlPhoto = URL(string: photo.urls?.full ?? "https://i.imgur.com/tGbaZCY.jpg")
        let processor = DownsamplingImageProcessor(size: photoCell.photoImageView.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 0)
        photoCell.photoImageView.kf.indicatorType = .activity
        photoCell.photoImageView.kf.setImage(
            with: urlPhoto,
            options: [
                .processor(processor),
                .loadDiskFileSynchronously,
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        
        return photoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 3 - lay.minimumInteritemSpacing
        return CGSize(width: widthPerItem - 8, height: 230)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }
}

extension String {
    private var convertHtmlToNSAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data,options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func convertHtmlToAttributedStringWithCSS(font: UIFont? , csscolor: String , lineheight: Int, csstextalign: String) -> NSAttributedString? {
        guard let font = font else {
            return convertHtmlToNSAttributedString
        }
        let modifiedString = "<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px; color: \(csscolor); line-height: \(lineheight)px; text-align: \(csstextalign); }</style>\(self)";
        guard let data = modifiedString.data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error)
            return nil
        }
    }
}
