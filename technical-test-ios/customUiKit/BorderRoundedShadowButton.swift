//
//  BorderRoundedShadowButton.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit

class BorderRoundedShadowButton: UIButton {
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 6).cgPath
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.8
        self.layer.cornerRadius = 10.0
    }
    
}
