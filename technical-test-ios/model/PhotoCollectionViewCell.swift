//
//  PhotoCollectionViewCell.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userImage: RoundedWithoutBorderImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var totalLikesLabel: UILabel!
    @IBOutlet weak var addFavoriteButton: UIButton!
}
