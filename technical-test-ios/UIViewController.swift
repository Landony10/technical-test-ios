//
//  UIViewController.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit
import PKHUD
import NotificationBannerSwift

extension UIViewController {
    func showLoader() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    func hideLoader() {
        PKHUD.sharedHUD.hide()
    }
    
    func showBanner(title: String, message: String, style: BannerStyle) {
        let banner = NotificationBanner(title: title, subtitle: message, style: style)
        banner.show()
    }
}
