//
//  ImageDatabase.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import RealmSwift

class ImageDatabase: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var urls: UrlsDatabase?
    @objc dynamic var likes: Int = 0
    @objc dynamic var user: UserDatabase?
    @objc dynamic var descriptionImage: String = ""
    @objc dynamic var alt_description: String = ""
    @objc dynamic var created_at: String = ""
    @objc dynamic var width: Int = 1000
    @objc dynamic var height: Int = 1000
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class UserDatabase: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var profile_image: ProfileImageDatabase?
    @objc dynamic var username: String = ""
    @objc dynamic var updatedAt: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var twitterUsername: String = ""
    @objc dynamic var portfolioURL: String = ""
    @objc dynamic var bio: String = ""
    @objc dynamic var location: String = ""
    @objc dynamic var instagramUsername: String = ""
    @objc dynamic var totalCollections: Int64 = 0
    @objc dynamic var totalLikes: Int64 = 0
    @objc dynamic var totalPhotos: Int64 = 0
    @objc dynamic var acceptedTos: Bool = false
    @objc dynamic var forHire: Bool = false
    var photos = Array<PhotosUserDatabase>()
    @objc dynamic var followers_count: Int = 0
    @objc dynamic var following_count: Int = 0
}

class ProfileImageDatabase: Object {
    @objc dynamic var medium: String = ""
}

class PhotosUserDatabase: Object {
    @objc dynamic var urls: UrlsDatabase?
}

class UrlsDatabase: Object {
    @objc dynamic var full: String = ""
}
