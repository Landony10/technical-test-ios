//
//  FavoriteViewController.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit
import RealmSwift
import NotificationBannerSwift
import Kingfisher

class FavoriteViewController: UIViewController {
    
    @IBOutlet weak var withoutImagesView: UIView!
    @IBOutlet weak var favoriteCollectionView: UICollectionView!
    
    private let realm = try! Realm()
    private var images = try! Realm().objects(ImageDatabase.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 4
        favoriteCollectionView
            .setCollectionViewLayout(layout, animated: true)
        self.fillImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.images = self.realm.objects(ImageDatabase.self)
        print("IMAGES: \(self.images)")
        if !self.images.isEmpty {
            self.withoutImagesView.isHidden = true
            self.favoriteCollectionView.isHidden = false
            self.favoriteCollectionView.reloadData()
        }
    }
    
    private func fillImages() {
        if !self.images.isEmpty {
            self.withoutImagesView.isHidden = true
            self.favoriteCollectionView.isHidden = false
            self.favoriteCollectionView.reloadData()
        } else {
            self.favoriteCollectionView.isHidden = true
            self.withoutImagesView.isHidden = false
        }
    }
    
    private func deleteImage(image: ImageDatabase) {
        try! self.realm.write {
            self.realm.delete(image)
        }
        self.images = self.realm.objects(ImageDatabase.self)
        if self.images.isEmpty {
            self.favoriteCollectionView.isHidden = true
            self.withoutImagesView.isHidden = false
        }
        self.hideLoader()
        self.favoriteCollectionView.reloadData()
        self.showBanner(title: "EXITO", message: "Se elimino de tus favoritos de forma correcta.", style: .success)
    }
}

extension FavoriteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCollectionViewCell
        let photo = images[indexPath.row]
        let processor = DownsamplingImageProcessor(size: photoCell.photoImage.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 0)
        photoCell.photoImage.kf.indicatorType = .activity
        
        let urlPhoto = URL(string: photo.urls?.full ?? "https://i.imgur.com/tGbaZCY.jpg")
        let urlPhotoUser = URL(string: photo.user?.profile_image?.medium ?? "https://i.imgur.com/tGbaZCY.jpg")
        
        photoCell.userNameLabel.text = photo.user?.username ?? "Error al cargar"
        photoCell.totalLikesLabel.text = "\(photo.likes ) Me gusta"
        photoCell.photoImage.kf.setImage(
            with: urlPhoto,
            options: [
                .processor(processor),
                .loadDiskFileSynchronously,
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        photoCell.userImage.kf.setImage(with: urlPhotoUser!)
        
        let gestureAddFavorite = UITapGestureRecognizer(target: self, action: #selector(self.deleteFavoriteImage(gesture:)))
        photoCell.addFavoriteButton.tag = indexPath.row
        photoCell.addFavoriteButton.isUserInteractionEnabled = true
        photoCell.addFavoriteButton.addGestureRecognizer(gestureAddFavorite)
        
        return photoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        return CGSize(width: widthPerItem - 8, height: 225)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected")
        let photo = images[indexPath.row]
        let user = User()
        let profileImage = ProfileImage()
        profileImage.medium = photo.user?.profile_image?.medium
        user.id = photo.user?.id
        user.name = photo.user?.name
        user.profile_image = profileImage
        user.username = photo.user?.username
        user.updatedAt = photo.user?.updatedAt
        user.firstName = photo.user?.firstName
        user.lastName = photo.user?.lastName
        user.twitterUsername = photo.user?.twitterUsername
        user.portfolioURL = photo.user?.portfolioURL
        user.bio = photo.user?.bio
        user.location = photo.user?.location
        user.instagramUsername = photo.user?.instagramUsername
        user.totalCollections = photo.user?.totalCollections
        user.totalLikes = photo.user?.totalLikes
        user.totalPhotos = photo.user?.totalPhotos
        user.acceptedTos = ((photo.user?.acceptedTos) != nil)
        user.forHire = photo.user?.forHire
        user.followers_count = photo.user?.followers_count ?? 0
        user.following_count = photo.user?.following_count ?? 0
        
        DispatchQueue.main.async {
            let storyBoard = UIStoryboard(name: "User", bundle:nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "UserView") as! UserViewController
            viewController.user = user
            viewController.modalPresentationStyle = .overFullScreen
            self.present(viewController, animated:true, completion:nil)
        }
    }
    
    @objc func deleteFavoriteImage(gesture: UIGestureRecognizer) {
        self.showLoader()
        let view = gesture.view!
        let imageSelected = self.images[view.tag]
        self.deleteImage(image: imageSelected)
    }
}

