//
//  ImageViewExtension.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.contentMode = .scaleAspectFill
                        self?.image = image
                    }
                }
            }
        }
    }
}
