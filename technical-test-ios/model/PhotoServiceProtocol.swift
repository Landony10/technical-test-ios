//
//  PhotoServiceProtocol.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import Alamofire
import SwiftyJSON

protocol PhotoServiceProtocol {
    func getPhotos(page: Int, responsePhotos: @escaping ([ImageModel]?, Int) -> ())
}
