//
//  HomeViewController.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit
import Kingfisher
import RealmSwift

class HomeViewController: UIViewController {

    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    internal var photoService: PhotoService!
    
    private var images: [ImageModel]?
    
    private var page: Int = 1
    private var isLoading = false
    private let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 4
        photosCollectionView
            .setCollectionViewLayout(layout, animated: true)
        self.images = []
        self.getPhotos()
    }
    
    private func getPhotos() {
        self.showLoader()
        self.photoService.getPhotos(page: self.page) { data, statusCode in
            self.proccessPhotos(data: data)
        }
    }
    
    private func proccessPhotos(data: [ImageModel]?) {
        if data != nil && !data!.isEmpty {
            for image in data! {
                self.images?.append(image)
            }
            self.hideLoader()
            self.photosCollectionView.reloadData()
            self.isLoading = false
        }
    }
    
    private func insertOrUpdateMessage(image: ImageModel) {
        print("IMAGE: \(image.toJSON())")
        let imageDatabase = ImageDatabase()
        
        let urlDatabase = UrlsDatabase()
        urlDatabase.full = image.urls!.full!
        
        let profileImageDatabase = ProfileImageDatabase()
        profileImageDatabase.medium = image.user!.profile_image!.medium!
        
        var photosUser: Array<PhotosUserDatabase> = []
        
        let userDatabase = UserDatabase()
        userDatabase.id = image.user?.id ?? ""
        userDatabase.name = image.user?.name ?? ""
        userDatabase.profile_image = profileImageDatabase
        userDatabase.username = image.user?.username ?? ""
        userDatabase.updatedAt = image.user?.updatedAt ?? ""
        userDatabase.firstName = image.user?.firstName ?? ""
        userDatabase.lastName = image.user?.lastName ?? ""
        userDatabase.twitterUsername = image.user?.twitterUsername ?? ""
        userDatabase.portfolioURL = image.user?.portfolioURL ?? ""
        userDatabase.bio = image.user?.bio ?? ""
        userDatabase.location = image.user?.location ?? ""
        userDatabase.instagramUsername = image.user?.instagramUsername ?? ""
        userDatabase.totalCollections = image.user?.totalCollections ?? 0
        userDatabase.totalLikes = image.user?.totalLikes ?? 0
        userDatabase.totalPhotos = image.user?.totalPhotos ?? 0
        userDatabase.acceptedTos = image.user?.acceptedTos ?? false
        userDatabase.forHire = image.user?.forHire ?? false
        userDatabase.followers_count = image.user?.followers_count ?? 0
        userDatabase.following_count = image.user?.following_count ?? 0
        
        if !(image.user?.photos?.isEmpty ?? true) {
            for photo in image.user!.photos! {
                let photoDatabase = PhotosUserDatabase()
                let urlDb = UrlsDatabase()
                urlDb.full = photo.urls!.full!
                photoDatabase.urls = urlDb
                photosUser.append(photoDatabase)
            }
        }
        userDatabase.photos = photosUser
        
        imageDatabase.id = image.id!
        imageDatabase.urls = urlDatabase
        imageDatabase.likes = image.likes!
        imageDatabase.user = userDatabase
        imageDatabase.descriptionImage = image.description ?? ""
        imageDatabase.alt_description = image.alt_description ?? ""
        imageDatabase.created_at = image.created_at ?? ""
        imageDatabase.width = image.width
        imageDatabase.height = image.height
        
        try! self.realm.write {
            self.realm.add(imageDatabase, update: .modified)
        }
        self.showBanner(title: "EXITO", message: "Se agrego a favoritos de forma correcta.", style: .success)
        self.hideLoader()
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCollectionViewCell
        let photo = images?[indexPath.row]
        let processor = DownsamplingImageProcessor(size: photoCell.photoImage.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 0)
        photoCell.photoImage.kf.indicatorType = .activity
        
        let urlPhoto = URL(string: photo?.urls?.full ?? "https://i.imgur.com/tGbaZCY.jpg")
        let urlPhotoUser = URL(string: photo?.user?.profile_image?.medium ?? "https://i.imgur.com/tGbaZCY.jpg")
        
        photoCell.userNameLabel.text = photo?.user?.username ?? "Error al cargar"
        photoCell.totalLikesLabel.text = "\(photo?.likes ?? 0) Me gusta"
        
        photoCell.photoImage.kf.setImage(
            with: urlPhoto,
            options: [
                .processor(processor),
                .loadDiskFileSynchronously,
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        photoCell.userImage.kf.setImage(with: urlPhotoUser!)
        
        let gestureAddFavorite = UITapGestureRecognizer(target: self, action: #selector(self.addFavoriteImage(gesture:)))
        photoCell.addFavoriteButton.tag = indexPath.row
        photoCell.addFavoriteButton.isUserInteractionEnabled = true
        photoCell.addFavoriteButton.addGestureRecognizer(gestureAddFavorite)
        
        return photoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        return CGSize(width: widthPerItem - 15, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                     layout collectionViewLayout: UICollectionViewLayout,
                     insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
       }
    
    internal func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling = (scrollView.contentOffset.y + scrollView.frame.size.height)
        
        if endScrolling >= scrollView.contentSize.height {
            if !self.isLoading {
                self.isLoading = true
                self.page+=1
                DispatchQueue.main.async {
                    self.getPhotos()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = images?[indexPath.row]
        
        DispatchQueue.main.async {
            let storyBoard = UIStoryboard(name: "User", bundle:nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "UserView") as! UserViewController
            viewController.user = photo?.user
            viewController.modalPresentationStyle = .overFullScreen
            self.present(viewController, animated:true, completion:nil)
        }
    }
    
    @objc func addFavoriteImage(gesture: UIGestureRecognizer) {
        self.showLoader()
        let view = gesture.view!
        let imageSelected = self.images?[view.tag]
        if imageSelected != nil {
            self.insertOrUpdateMessage(image: imageSelected!)
        }
    }
}
