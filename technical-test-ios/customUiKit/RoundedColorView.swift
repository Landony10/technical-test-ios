//
//  RoundedColorView.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import Foundation

import UIKit

class RoundedColorView: UIView {
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.layer.cornerRadius = 6.0
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 194/255, green: 181/255, blue: 155/255, alpha: 1.0).cgColor
    }
    
}

