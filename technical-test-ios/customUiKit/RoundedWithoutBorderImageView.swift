//
//  RoundedWithoutBorderImageView.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import UIKit

class RoundedWithoutBorderImageView: UIImageView {
    public required init(coder: NSCoder) {
        super.init(coder: coder)!
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
