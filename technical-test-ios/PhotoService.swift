//
//  PhotoService.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import Alamofire
import AlamofireObjectMapper
import SwiftyJSON

struct PhotoService: PhotoServiceProtocol {
    func getPhotos(page: Int, responsePhotos: @escaping ([ImageModel]?, Int) -> ()) {
        var statusCode: Int?
        var result: [ImageModel]?
        
        let url = "\(TagReference.tagUrlBase)\(TagReference.tagUrlPhotos)?page=\(page)"
        
        print("url \(url)")
        
        let headers = [
            "Authorization": "Client-ID Cro7SsifAJcV7ubSV2fOLCwuufypmeQp-uV1Oxu-h_E"
        ]
                
        Alamofire.request(url, method: .get, headers: headers)
            .validate(statusCode: 200..<500)
            .responseArray {(response: DataResponse<[ImageModel]>) in
                switch response.result{
                case .success:
                    result = response.value
                case .failure(_):
                    result = nil
                }
                statusCode = response.response?.statusCode ?? 0
                print("url \(result ?? []) - \(statusCode!)")
                responsePhotos(result, statusCode!)
            }
    }
}
