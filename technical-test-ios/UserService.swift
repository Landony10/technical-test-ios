//
//  UserService.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import Alamofire
import AlamofireObjectMapper
import SwiftyJSON

struct UserService: UserServiceProtocol {
    func getUser(user: String, responseUser: @escaping (User?, Int) -> ()) {
        var statusCode: Int?
        var result: User?
        
        let url = "\(TagReference.tagUrlBase)\(TagReference.tagUrlUser)\(user)"
        
        print("url \(url)")
        
        let headers = [
            "Authorization": "Client-ID Cro7SsifAJcV7ubSV2fOLCwuufypmeQp-uV1Oxu-h_E"
        ]
                
        Alamofire.request(url, method: .get, headers: headers)
            .validate(statusCode: 200..<500)
            .responseObject{(response: DataResponse<User>) in
                switch response.result{
                case .success:
                    result = response.value
                case .failure(_):
                    result = nil
                }
                statusCode = response.response?.statusCode ?? 0
                print("url \(result) - \(statusCode!)")
                responseUser(result, statusCode!)
            }
    }
}
