//
//  ImageModel.swift
//  technical-test-ios
//
//  Created by Landony Aguilar on 23/09/21.
//

import ObjectMapper

class ImageModel: Mappable {
    public var id: String?
    public var urls: Urls?
    public var likes: Int?
    public var user: User?
    public var description: String?
    public var alt_description: String?
    public var created_at: String?
    public var width: Int = 1000
    public var height: Int = 1000
    
    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        urls <- map["urls"]
        likes <- map["likes"]
        user <- map["user"]
        description <- map["description"]
        alt_description <- map["alt_description"]
        created_at <- map["created_at"]
        width <- map["width"]
        height <- map["height"]
    }
}

class User: Mappable {
    public var id: String?
    public var name: String?
    public var profile_image: ProfileImage?
    public var username: String?
    public var updatedAt: String?
    public var firstName: String?
    public var lastName: String?
    public var twitterUsername: String?
    public var portfolioURL: String?
    public var bio: String?
    public var location: String?
    public var instagramUsername: String?
    public var totalCollections: Int64?
    public var totalLikes: Int64?
    public var totalPhotos: Int64?
    public var acceptedTos: Bool = false
    public var forHire: Bool?
    public var photos: Array<PhotosUser>?
    public var followers_count: Int = 0
    public var following_count: Int = 0
    
    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        profile_image <- map["profile_image"]
        username <- map["username"]
        updatedAt <- map["updated_at"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        twitterUsername <- map["twitter_username"]
        portfolioURL <- map["portfolio_url"]
        bio <- map["bio"]
        location <- map["location"]
        instagramUsername <- map["instagram_username"]
        totalCollections <- map["total_collections"]
        totalLikes <- map["total_likes"]
        totalPhotos <- map["total_photos"]
        acceptedTos <- map["accepted_tos"]
        forHire <- map["for_hire"]
        photos <- map["photos"]
        followers_count <- map["followers_count"]
        following_count <- map["following_count"]
    }
}

class ProfileImage: Mappable {
    var medium: String?
    
    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        medium <- map["medium"]
    }
}

class PhotosUser: Mappable {
    var urls: Urls?
    
    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        urls <- map["urls"]
    }
}

class Urls: Mappable {
    var full: String?
    
    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        full <- map["full"]
    }
}

