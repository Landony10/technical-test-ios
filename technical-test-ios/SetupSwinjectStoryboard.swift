//
//  SetupSwinjectStoryboard.swift
//  technical-test-ios (iOS)
//
//  Created by Landony Aguilar on 22/09/21.
//

import SwinjectStoryboard

extension SwinjectStoryboard {
    class func setup() {
        defaultContainer.storyboardInitCompleted(HomeViewController.self) { request, controller in
            controller.photoService = request.resolve(PhotoService.self)
        }
        
        defaultContainer.storyboardInitCompleted(UserViewController.self) { request, controller in
            controller.userService = request.resolve(UserService.self)
        }
        
        defaultContainer.register(PhotoService.self) { _ in PhotoService() }
        defaultContainer.register(UserService.self) { _ in UserService() }
    }
}
